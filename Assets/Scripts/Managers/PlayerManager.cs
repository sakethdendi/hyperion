using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    RaycastHit hit;
    // Update is called once per frame

    List<UnitController> selectedUnits = new List<UnitController>();
    bool isDragging = false;
    Vector3 mousePosition;
    
    private void OnGUI() {  

        if (isDragging) {
            var rect = ScreenHelper.GetScreenRect(mousePosition, Input.mousePosition);
            ScreenHelper.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.1f));
            ScreenHelper.DrawScreenRectBorder(rect, 1, Color.blue);
        }
        
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {
            mousePosition = Input.mousePosition;
            var camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(camRay, out hit)) {
                Debug.Log(hit.transform);
            }

            if(hit.transform.CompareTag("Unit")) {
                SelectUnit(hit.transform.GetComponent<UnitController>(), Input.GetKey(KeyCode.LeftShift));
            } 
            else {
                isDragging = true;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if(isDragging) {
                DeselectUnits();
                GameObject[] gos = GameObject.FindGameObjectsWithTag("Unit");
                foreach (var selectableObject in gos)
                {
                    if (IsWithinSelectionBounds(selectableObject.transform))
                    {
                        SelectUnit(selectableObject.gameObject.GetComponent<UnitController>(), true);
                    }
                }
            }
            isDragging = false;
   
        }

        if (Input.GetMouseButtonDown(1) && selectedUnits.Count > 0) {
            var camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(camRay, out hit)) {
                Debug.Log(hit.transform);
                if(hit.transform.CompareTag("Ground")) {
                    foreach(var selectableObject in selectedUnits) {
                        selectableObject.MoveUnit(hit.point);
                        selectableObject.navAgent.stoppingDistance = 2;
                    }
                }
            
                else if (hit.transform.CompareTag("EnemyUnit")) {
                    foreach(var selectableObject in selectedUnits) {
                        selectableObject.SetNewTarget(hit.transform);
                        selectableObject.navAgent.stoppingDistance = 7;
                    }
                }
            }
        }
    }
    

    private void SelectUnit(UnitController unit, bool isMultiSelect = false) {

        if (!isMultiSelect) {
            DeselectUnits();
        }   

        selectedUnits.Add(unit);
        unit.SetSelected(true);
    }

    private void DeselectUnits() {
        for(int i = 0; i < selectedUnits.Count; i++) {
           // selectedUnits[i].Find("Highlight").gameObject.SetActive(false);
           selectedUnits[i].SetSelected(false);
        }
        selectedUnits.Clear();

    }

    private bool IsWithinSelectionBounds(Transform transform) {

        if(!isDragging) {
            return false;
        }
        var camera = Camera.main;
        var viewportBounds = ScreenHelper.GetViewportBounds(camera, mousePosition, Input.mousePosition);
        return viewportBounds.Contains(camera.WorldToViewportPoint(transform.position));
    }
}
