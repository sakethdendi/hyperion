using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HyperionGameManager : MonoBehaviour {

    public static void UnitTakeDamage(UnitController attackingController, UnitController attackedController) {
        var damage = attackingController.unitStats.attackDamage;
        attackedController.TakeDamage(damage);
    }
}
