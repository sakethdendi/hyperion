using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtFollow : MonoBehaviour
{
    public Transform mTarget;
    float maxSpeed = 10.0f;
    Vector3 mLookDirection;
    const float EPSILON = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mLookDirection = (mTarget.position - transform.position).normalized;
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
            if (Physics.Raycast(ray, out hit)) {
                if (hit.transform.name == "Movable Object") {
                    if ((transform.position - mTarget.position).magnitude > EPSILON) {
                        transform.Translate(mLookDirection * Time.deltaTime * maxSpeed);
                    }
                }
            }
       }
    
}
