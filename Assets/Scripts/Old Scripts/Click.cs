using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click : MonoBehaviour
{
    
    public RectTransform selectionBox;
    [SerializeField]
    private LayerMask clickableLayer;
    private Vector2 startPos;

    private List<GameObject> selectedObjects;

    void Start() 
    {
        selectedObjects = new List<GameObject>();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1)) {
            if (selectedObjects.Count > 0) {
                foreach (GameObject obj in selectedObjects) {
                    obj.GetComponent<ClickOn>().currentlySelected = false;
                    obj.GetComponent<ClickOn>().ClickMe();
                    }
            selectedObjects.Clear();
            }
        }

        if (Input.GetMouseButtonDown(0)) {
            RaycastHit rayHit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, clickableLayer)) {
                ClickOn clickOnScript = rayHit.collider.GetComponent<ClickOn>();

                if (Input.GetKey("left ctrl")) {
                    if (clickOnScript.currentlySelected == false) {
                        selectedObjects.Add(rayHit.collider.gameObject);
                        clickOnScript.currentlySelected = true;
                        clickOnScript.ClickMe();
                    }
                    else {
                        selectedObjects.Remove(rayHit.collider.gameObject);
                        clickOnScript.currentlySelected = false;
                        clickOnScript.ClickMe();
                    }
                }
                else {
                    if (selectedObjects.Count > 0) {
                        foreach (GameObject obj in selectedObjects) {
                            obj.GetComponent<ClickOn>().currentlySelected = false;
                            obj.GetComponent<ClickOn>().ClickMe();
                        }
                        selectedObjects.Clear();
                    }
                    selectedObjects.Add(rayHit.collider.gameObject);
                    clickOnScript.currentlySelected = true;
                    clickOnScript.ClickMe();
                }
            }
            startPos = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0)) {

        }

        if (Input.GetMouseButtonDown(0)) {
            UpdateSelectionBox(Input.mousePosition);
        }
    }

    void UpdateSelectionBox(Vector2 curMousePosition) {
        if(!selectionBox.gameObject.activeInHierarchy) {
            selectionBox.gameObject.SetActive(true);
        }
        float width = curMousePosition.x - startPos.x;
        float height = curMousePosition.y - startPos.y;

        selectionBox.sizeDelta = new Vector2(Mathf.Abs(width), Mathf.Abs(height));
        selectionBox.anchoredPosition = startPos + new Vector2(width / 2, height / 2);
    }
}
