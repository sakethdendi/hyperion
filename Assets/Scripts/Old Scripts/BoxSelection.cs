using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField] Box box;
    private Vector3 startPos,dragPos;
    private Camera cam;
    private Ray ray;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(1)) {

            RaycastHit hit;
            ray = cam.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit, 100f);

            if (Input.GetMouseButtonDown(1)) {
                startPos = hit.point;
                box.baseMin = startPos;


            }

            dragPos = hit.point;
            box.baseMax = dragPos;
        }
        else if(Input.GetMouseButtonUp(1)) {

        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(box.Center, box.Size);
    }
}

[System.Serializable]
public class Box {
    public Vector3 baseMin, baseMax;

    public Vector3 Center {
        get {
            Vector3 center = baseMin + (baseMax - baseMin) * 0.5f;
            center.y = (baseMax - baseMin).magnitude * 0.5f;
            return center;
        }
    }

    public Vector3 Size {
        get {
            return new Vector3(Mathf.Abs(baseMax.x - baseMin.x), (baseMax - baseMin).magnitude, Mathf.Abs(baseMax.z - baseMin.z));
        }
    }

    public Vector3 Extents {
        get {
            return Size * 0.5f;
        }
    }
}

