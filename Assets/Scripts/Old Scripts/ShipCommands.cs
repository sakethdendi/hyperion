using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCommands : MonoBehaviour
{
    public GameObject selectedObject;
    private ShipCommands selectedInfo;
    private UnityEngine.AI.NavMeshAgent agent;
    public bool isSelected = false;
    public Transform mTarget;
    float maxSpeed = 10.0f;
    Vector3 mLookDirection;
    const float EPSILON = 0.1f;
    public string objectName;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RightClick() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        mLookDirection = (mTarget.position - transform.position).normalized;


        if (Physics.Raycast(ray, out hit, 100)) {
            if(hit.collider.tag == "Ground") {
                agent.destination = hit.point;
                Debug.Log("Moving");
            }
                if(hit.collider.tag == "Movable") {
                if ((transform.position - mTarget.position).magnitude > EPSILON) {
                    transform.Translate(mLookDirection * Time.deltaTime * maxSpeed);
                }
            }
        }
    }
    public void LeftClick() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100)) {
            if (hit.collider.tag == "Ground")  {
                selectedObject = null;
                selectedInfo.isSelected = false;
                Debug.Log("Deselected");
            }
            else if(hit.collider.tag == "Unit") {
                selectedObject = hit.collider.gameObject;
                selectedInfo = selectedObject.GetComponent<ShipCommands>();

                selectedInfo.isSelected = true;
                Debug.Log("Selected" + selectedInfo.objectName);

            }
        }
    }
}
