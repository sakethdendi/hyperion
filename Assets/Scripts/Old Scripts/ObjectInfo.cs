using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInfo : MonoBehaviour
{
    public string objectName;
    private UnityEngine.AI.NavMeshAgent agent;
    public Renderer[] renderers; //Assign all child Mesh Renderers
    private ObjectInfo selectedInfo;
    public bool box = false;

    public Transform mTarget;
    float maxSpeed = 10.0f;
    Vector3 mLookDirection;
    const float EPSILON = 0.1f;
    public ShipCommands com;



    // Start is called before the first frame update
    void Start()    
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetMouseButtonDown(0)) {
            com.LeftClick();
        }
        if (Input.GetMouseButtonDown(1) && com.isSelected == true) {
            com.RightClick(); 
        }

    }
/*
    public void RightClick() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        mLookDirection = (mTarget.position - transform.position).normalized;


        if (Physics.Raycast(ray, out hit, 100)) {
            if(hit.collider.tag == "Ground") {
                agent.destination = hit.point;
                Debug.Log("Moving");
            }
                if(hit.collider.tag == "Movable") {
                if ((transform.position - mTarget.position).magnitude > EPSILON) {
                    transform.Translate(mLookDirection * Time.deltaTime * maxSpeed);
                }
            }
        }
    }
    public void LeftClick() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100)) {
            if (hit.collider.tag == "Ground")  {
                selectedObject = null;
                selectedInfo.isSelected = false;
                Debug.Log("Deselected");
            }
            else if(hit.collider.tag == "Unit") {
                selectedObject = hit.collider.gameObject;
                selectedInfo = selectedObject.GetComponent<ObjectInfo>();

                selectedInfo.isSelected = true;
                Debug.Log("Selected" + selectedInfo.objectName);

            }
        }
    } */
    public Bounds GetObjectBounds()
    {
        Bounds totalBounds = new Bounds();

        for(int i = 0; i < renderers.Length; i++)
        {
            if(totalBounds.center == Vector3.zero)
            {
                totalBounds = renderers[i].bounds;
            }
            else
            {
                totalBounds.Encapsulate(renderers[i].bounds);
            }
        }

        return totalBounds;
    }

    void OnEnable()
    {
        //Add this Object to global list
        if (!SC_SelectionManager.selectables.Contains(this))
        {
            SC_SelectionManager.selectables.Add(this);
            box = true;
        }
    }

    void OnDisable()
    {
        //Remove this Object from global list
        if (SC_SelectionManager.selectables.Contains(this))
        {
            SC_SelectionManager.selectables.Remove(this);
            box = false;
        }
    }
}
