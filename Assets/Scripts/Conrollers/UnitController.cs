using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitController : MonoBehaviour
{
    // Start is called before the first frame update
    public NavMeshAgent navAgent;
    private Transform currentTarget;
    public UnitStats unitStats;
    private float attackTimer;
    public bool isDead = false;
    public ParticleSystem laser;
    public float rotationSpeed = 10f;
    public ParticleSystem explosion;

    public void Start() {
        navAgent = GetComponent<NavMeshAgent>();
        attackTimer = unitStats.attackSpeed;
        laser.Stop();
        explosion.Stop();
        unitStats.health = unitStats.startingHealth;
    }

    private void Update() {
        attackTimer += Time.deltaTime;
        if (currentTarget != null) {
            navAgent.destination = currentTarget.position;

            var distance = (transform.position - currentTarget.position).magnitude;

            if (currentTarget.GetComponent<UnitController>().isDead) {
                laser.Stop();
                SetNoTarget();
            }

            if (distance <= unitStats.attackRange && isDead != true) {
                RotateTowards(currentTarget);
                Attack();
            }
        }
    }
    public void MoveUnit(Vector3 dest) {
        currentTarget = null;
        navAgent.destination = dest;
        //add navagent.stoppingDistance
    }

    public void SetSelected(bool isSelected) {
        transform.Find("Highlight").gameObject.SetActive(isSelected);
    }

    public void SetNewTarget(Transform enemy) {
        currentTarget = enemy;
    }

    public void SetNoTarget() {
        currentTarget = null;
    }

    public void Attack() {
        if (attackTimer >= unitStats.attackSpeed) {
            HyperionGameManager.UnitTakeDamage(this, currentTarget.GetComponent<UnitController>());
            attackTimer = 0;
            laser.Play();
        }
    }

    public void TakeDamage(float damage) {
        if (unitStats.health < damage) {
            unitStats.health = 0;
            explosion.Play();
            isDead = true;
            laser.Stop();
        }

        else {
            StartCoroutine(Flasher(GetComponent<Renderer>().material.color));
            unitStats.health = unitStats.health - damage;
        }
    }

    IEnumerator Flasher(Color defaultColor) {
        var renderer = GetComponent<Renderer>();
        for (int i = 0; i < 2; i++) {
            renderer.material.color = Color.gray;
            yield return new WaitForSeconds(.05f);
            renderer.material.color = defaultColor;
            yield return new WaitForSeconds(.05f);
        }           
    }

    public void RotateTowards (Transform target) {
            Vector3 direction = (target.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
     }
}
